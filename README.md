Ralph Hackett is 67 years old and lives in Clovis, California with Lisa, his wife of 37 years. With an illustrious career spanning 44 years, Ralph has served on a number of industry boards and actively supported a variety of charitable organizations.

website : http://ralphhackett.com